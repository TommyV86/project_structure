package fr.afpa.dao;

import fr.afpa.utilitary.HibernateUtilitary;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ObjectDao<T> {
    private Class<T> entity;
    public ObjectDao(Class<T> entity){
        this.entity = entity;
    }
    static Session session = HibernateUtilitary.getSessionFactory().openSession();

    public void create(T t) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(t);
            transaction.commit();
            System.out.println(transaction);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public T findById(Long id) {
        T t = (T) session.get(entity, id);
        return t;
    }

    public List<T> findAll() {
        Criteria criteria = session.createCriteria(entity);
        List<T> t = (List<T>) criteria.list();

        return t;
    }

    public void update(T t) {
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();

            T r = (T) session.merge(t);

            session.saveOrUpdate(r);
            transaction.commit();
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    public void delete(T t) {
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();

            T r = (T) session.merge(t);
            session.delete(r);
            transaction.commit();
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
