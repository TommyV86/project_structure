package fr.afpa.dao.namedQuery;

import fr.afpa.model.BankAccount;
import fr.afpa.utilitary.HibernateUtilitary;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

public class BankAccountNQDao {
    static Session session = HibernateUtilitary.getSessionFactory().openSession();

    public List<BankAccount> findAllById(Long idClient){
        session.clear();

        Query query = session.getNamedQuery("findAllById");
        query.setParameter("idClient", idClient);

        return (List<BankAccount>) query.list();
    }
}
