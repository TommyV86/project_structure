package fr.afpa.dao.extension;

import fr.afpa.dao.ObjectDao;
import fr.afpa.model.BankAccount;

public class BankAccountDao extends ObjectDao<BankAccount> {
    public BankAccountDao(){
        super(BankAccount.class);
    }
}
