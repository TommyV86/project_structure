package fr.afpa.dao.extension;

import fr.afpa.dao.ObjectDao;
import fr.afpa.model.Client;

public class ClientDao extends ObjectDao<Client> {
    public ClientDao(){
        super(Client.class);
    }
}
