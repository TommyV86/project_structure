package fr.afpa.dao.extension;

import fr.afpa.dao.ObjectDao;
import fr.afpa.model.Agency;

public class AgencyDao extends ObjectDao<Agency> {
    public AgencyDao(){
        super(Agency.class);
    }
}
