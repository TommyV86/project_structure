package fr.afpa.dao.extension;

import fr.afpa.dao.ObjectDao;
import fr.afpa.model.TypeAccount;

public class TypeAccountDao extends ObjectDao<TypeAccount> {
    public TypeAccountDao(){
        super(TypeAccount.class);
    }
}
