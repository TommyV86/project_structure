package fr.afpa.test;

import fr.afpa.model.Agency;
import fr.afpa.model.BankAccount;
import fr.afpa.model.Client;
import fr.afpa.service.AgencyService;
import fr.afpa.service.BankAccountService;
import fr.afpa.service.ClientService;
import fr.afpa.service.TypeAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestBankAccount {
    BankAccountService bankAccountService = new BankAccountService();
    AgencyService agencyService = new AgencyService();
    ClientService clientService = new ClientService();
    TypeAccountService typeAccountService = new TypeAccountService();


    @Test
    void create(){
        Agency agencyFinded = agencyService.find(1L);
        Client clientFinded = clientService.findById(12L);
        BankAccount bankAccountFinded;
        BankAccount bankAccountCreated =
                new BankAccount(
                        20.0,
                        typeAccountService.findById(1L),
                        false,
                        clientFinded,
                        agencyFinded
                );

        bankAccountService.create(bankAccountCreated);
        bankAccountFinded = bankAccountService.findById(16L);

        Assertions.assertEquals(bankAccountCreated, bankAccountFinded);
    }

    @Test
    void find(){
        BankAccount bankAccountExpected = bankAccountService.findById(16L);
        BankAccount bankAccountFinded = bankAccountService.findById(bankAccountExpected.getIdBankAccount());

        Assertions.assertEquals(bankAccountExpected, bankAccountFinded);
    }

    @Test
    void findAll(){
        List<BankAccount> bankAccountsFinded = bankAccountService.findAll();

        Assertions.assertEquals(bankAccountsFinded.get(0).getIdBankAccount(), 12L);
    }

    @Test
    void findAllById(){
        List<BankAccount> clientsBankAccounts = bankAccountService.findAllById(3L);

        Assertions.assertEquals(clientsBankAccounts.get(0).getIdBankAccount(), 8);
    }

    @Test
    void update(){
        BankAccount bankAccountFinded = bankAccountService.findById(16L);
        Assertions.assertEquals(20.0, bankAccountFinded.getSolde());

        bankAccountFinded.setSolde(40.0);

        Assertions.assertEquals(40.0, bankAccountFinded.getSolde());
    }

    @Test
    void delete(){
        BankAccount bankAccountFinded = bankAccountService.findById(16L);
        bankAccountService.delete(bankAccountFinded);

        Assertions.assertEquals(null, bankAccountService.findById(16L));
    }
}
