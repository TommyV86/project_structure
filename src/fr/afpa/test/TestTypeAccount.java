package fr.afpa.test;

import fr.afpa.model.TypeAccount;
import fr.afpa.service.TypeAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTypeAccount {
    TypeAccountService typeAccountService = new TypeAccountService();

    @Test
    void find(){
        TypeAccount typeAccountExpected = typeAccountService.findById(1L);
        TypeAccount typeAccountFinded = typeAccountService.findById(typeAccountExpected.getIdTypeAccount());

        Assertions.assertEquals(typeAccountExpected, typeAccountFinded);
    }
}
