package fr.afpa.test;

import fr.afpa.model.Client;
import fr.afpa.service.ClientService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class TestClient {
    ClientService clientService = new ClientService();
    LocalDate localDate = LocalDate.now();

    @Test
    void create(){
        Client clientFinded;
        Client clientCreated =
                new Client("azerty", "seda", Date.valueOf(localDate), "momopo@bi");

        clientService.create(clientCreated);
        clientFinded = clientService.findById(17L);

        Assertions.assertEquals(clientFinded, clientCreated);
    }

    @Test
    void find(){
        Client clientExpected = clientService.findById(5L);
        Client clientFinded = clientService.findById(clientExpected.getIdClient());

        Assertions.assertEquals(clientExpected, clientFinded);
    }

    @Test
    void findAll(){
        List<Client> clientListFinded = clientService.findAll();

        Assertions.assertEquals(clientListFinded.get(0).getIdClient(), 4);
        Assertions.assertEquals(clientListFinded.get(1).getIdClient(), 5);
        Assertions.assertEquals(clientListFinded.get(2).getIdClient(), 6);
        Assertions.assertEquals(clientListFinded.get(3).getIdClient(), 3);
        Assertions.assertEquals(clientListFinded.get(4).getIdClient(), 2);
        Assertions.assertEquals(clientListFinded.get(5).getIdClient(), 12);
        Assertions.assertEquals(clientListFinded.get(6).getIdClient(), 14);
    }

    @Test
    void update(){

        Client clientFinded = clientService.findById(12L);
        Assertions.assertEquals("seda", clientFinded.getFirstName());

        clientFinded.setFirstName("newName");
        clientService.update(clientFinded);
        Assertions.assertEquals("newName", clientFinded.getFirstName());
    }

    @Test
    void delete(){

        Client clientFinded = clientService.findById(17L);
        clientService.delete(clientFinded);

        Assertions.assertEquals(null, clientService.findById(17L));
    }
}
