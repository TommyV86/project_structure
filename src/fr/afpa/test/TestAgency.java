package fr.afpa.test;

import fr.afpa.model.Agency;
import fr.afpa.service.AgencyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestAgency {
    AgencyService agencyService = new AgencyService();

    @Test
    void create(){
        Agency agencyFinded;
        Agency agencyCreated = new Agency("test", "rue test");

        agencyService.create(agencyCreated);
        agencyFinded = agencyService.find(4L);

        Assertions.assertEquals(agencyFinded, agencyCreated);
    }

    @Test
    void find(){
        Agency agencyExpected = agencyService.find(1L);
        Agency agencyFinded = agencyService.find(agencyExpected.getIdAgency());

        Assertions.assertEquals(agencyExpected, agencyFinded);
    }

    @Test
    void findAll(){

        List<Agency> clientListFinded = agencyService.findAll();

        Assertions.assertEquals(clientListFinded.get(0).getIdAgency(), 1);
        Assertions.assertEquals(clientListFinded.get(1).getIdAgency(), 2);
        Assertions.assertEquals(clientListFinded.get(2).getIdAgency(), 3);
        Assertions.assertEquals(clientListFinded.get(3).getIdAgency(), 4);
    }

    @Test
    void update(){
        Agency agencyFinded = agencyService.find(4L);
        Assertions.assertEquals("test", agencyFinded.getName());

        agencyFinded.setName("newName");
        agencyService.update(agencyFinded);
        Assertions.assertEquals("newName", agencyFinded.getName());
    }

    @Test
    void delete(){
        Agency agencyFinded = agencyService.find(4L);
        agencyService.delete(agencyFinded);

        Assertions.assertEquals(null, agencyService.find(4L));
    }
}
