package fr.afpa;

import fr.afpa.controller.AgencyController;
import fr.afpa.controller.AppController;
import fr.afpa.controller.BankAccountController;
import fr.afpa.controller.ClientController;

public class Main {
    private static final AppController appController = new AppController();
    private static final AgencyController agencyController = new AgencyController();
    private static final ClientController clientController = new ClientController();
    private static final BankAccountController bankAccountController = new BankAccountController();

    private static String choice;

    public static void main(String[] args){

        do {

            choice = appController.menu();

            switch (choice){
                case "a":
                    agencyController.create();
                    break;
                case "z":
                    agencyController.displayAll();
                    break;
                case "e":
                    agencyController.displayOne();
                    break;
                case "r":
                    agencyController.update();
                    break;
                case "t":
                    agencyController.delete();
                    break;
                case "q":
                    clientController.create();
                    break;
                case "s":
                    clientController.displayAll();
                    break;
                case "d":
                    clientController.displayOne();
                    break;
                case "f":
                    clientController.update();
                    break;
                case "g":
                    clientController.delete();
                    break;
                case "w":
                    bankAccountController.create();
                    break;
                case "x":
                    bankAccountController.displayAll();
                    break;
                case "c":
                    bankAccountController.displayAllById();
                    break;
                case "v":
                    bankAccountController.update();
                    break;
                case "b":
                    bankAccountController.delete();
                    break;
            }
        } while (!choice.equals("k"));

    }
}
