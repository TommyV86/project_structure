package fr.afpa.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class TypeAccount {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTypeAccount;
    @Column(unique = true)
    private String type;
    @OneToMany(mappedBy = "typeAccount")
    private List<BankAccount> bankAccounts;

    public Long getIdTypeAccount() {
        return idTypeAccount;
    }

    public void setIdTypeAccount(Long idTypeAccount) {
        this.idTypeAccount = idTypeAccount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
}
