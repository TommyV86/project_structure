package fr.afpa.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Agency {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAgency;
    @Column(unique = true)
    private String name;
    private String address;
    @OneToMany(mappedBy = "agency")
    private List<BankAccount> bankAccounts = new ArrayList<>();
    @OneToMany(mappedBy = "agency")
    private List<Client> clients = new ArrayList<>();

    public Agency() {}

    public Agency(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Long getIdAgency() {
        return idAgency;
    }

    public void setIdAgency(Long idAgency) {
        this.idAgency = idAgency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return  System.lineSeparator() +
                "Agency{" +
                "idAgency=" + idAgency +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}'
                + System.lineSeparator();
    }
}
