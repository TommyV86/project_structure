package fr.afpa.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

@Entity
public class Client {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idClient;
    @Column(unique = true)
    private String lastName;
    private String firstName;
    private Date birthDate;
    @Column(unique = true)
    private String mail;
    @OneToMany(mappedBy = "client", cascade = CascadeType.REMOVE)
    private List<BankAccount> bankAccounts = new ArrayList<>();
    @ManyToOne @JoinColumn(name = "idAgency")
    private Agency agency;

    public Client() {}

    public Client(String lastName, String firstName, Date birthDate, String mail) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.mail = mail;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public Agency getAgency() {
        return agency;
    }
    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @Override
    public String toString() {
        return  System.lineSeparator() +
                System.lineSeparator() +"Client{" +
                System.lineSeparator() +"idClient = " + idClient +
                System.lineSeparator() +"lastName = " + lastName +
                System.lineSeparator() +"firstName = " + firstName +
                System.lineSeparator() +"birthDate = " + birthDate +
                System.lineSeparator() +"mail = " + mail +
                System.lineSeparator() +"agency info = " + agency +
                System.lineSeparator() +'}'
                + System.lineSeparator();
    }
}
