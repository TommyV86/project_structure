package fr.afpa.model;

import javax.persistence.*;

@Entity @NamedQueries({
        @NamedQuery(name = "findAllById", query = "SELECT b FROM BankAccount b WHERE b.client.idClient = :idClient")
})
public class BankAccount {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBankAccount;
    private Double solde;
    @ManyToOne @JoinColumn(name = "idTypeAccount")
    private TypeAccount typeAccount;
    private Boolean isNegative;
    @ManyToOne @JoinColumn(name = "idClient")
    private Client client;
    @ManyToOne @JoinColumn(name = "idAgency")
    private Agency agency;

    public BankAccount() {}

    public BankAccount(Double solde, TypeAccount typeAccount, Boolean isNegative, Client client, Agency agency) {
        this.solde = solde;
        this.typeAccount = typeAccount;
        this.isNegative = isNegative;
        this.client = client;
        this.agency = agency;
    }

    public Long getIdBankAccount() {
        return idBankAccount;
    }

    public void setIdBankAccount(Long idBankAccount) {
        this.idBankAccount = idBankAccount;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }


    public TypeAccount getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(TypeAccount typeAccount) {
        this.typeAccount = typeAccount;
    }

    public Boolean getNegative() {
        return isNegative;
    }

    public void setNegative(Boolean negative) {
        isNegative = negative;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @Override
    public String toString() {
        return  System.lineSeparator() +
                System.lineSeparator() + "--------BankAccount--------{" +
                System.lineSeparator() + "idBankAccount=" + idBankAccount +
                System.lineSeparator() + "solde=" + solde +
                System.lineSeparator() + "type='" + typeAccount.getType() +
                System.lineSeparator() + "isNegative=" +  isNegative +
                System.lineSeparator() + "client=" + client +
                System.lineSeparator() + "agency=" + agency +
                '}'
                + System.lineSeparator();
    }
}
