package fr.afpa.controller;

import fr.afpa.view.AppView;

public class AppController {
    private final AppView appView = new AppView();

    public String menu(){
        return appView.menu();
    }
    public Long getId(){
        return appView.promptId();
    }
    public void getIdClientTitle(){
        appView.promptIdClientTitle();
    }
    public void getIdBankAccountTitle(){
        appView.promptIdBankAccountTitle();
    }
    public void getEnterMessage(){
        appView.enterMessageToContinue();
    }
    public void getTypeAccountTitle(){
        appView.promptTypeAccountTitle();
    }
}
