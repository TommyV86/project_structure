package fr.afpa.controller;

import fr.afpa.model.Agency;
import fr.afpa.service.AgencyService;
import fr.afpa.view.AgencyView;

import java.util.List;

public class AgencyController {
    private final AgencyService agencyService = new AgencyService();
    private final AgencyView agencyView = new AgencyView();
    private final AppController appController = new AppController();

    public void create(){
        agencyService.create(agencyView.promptCreate());
    }

    public List<Agency> findAll() {
        return agencyService.findAll();
    }
    public void displayAll(){
        findAll().forEach( a -> System.out.println(a));
        appController.getEnterMessage();
    }

    public Agency find(){
        return agencyService.find(appController.getId());
    }
    public void displayOne() {
        System.out.println(find());
        appController.getEnterMessage();
    }

    public void update() {
        agencyService.update(agencyView.promptUpdate(find()));
    }

    public void delete() {
        agencyService.delete(find());
    }

}
