package fr.afpa.controller;

import fr.afpa.model.Agency;
import fr.afpa.model.BankAccount;
import fr.afpa.model.Client;
import fr.afpa.model.TypeAccount;
import fr.afpa.service.BankAccountService;
import fr.afpa.view.BankAccountView;

import java.util.List;

public class BankAccountController {
    private final AppController appController = new AppController();
    private final BankAccountService bankAccountService = new BankAccountService();
    private final BankAccountView bankAccountView = new BankAccountView();
    private final ClientController clientController = new ClientController();
    private final TypeAccountController typeAccountController = new TypeAccountController();

    public void create(){
        Client client = clientController.findById();
        if (client == null ) return;
        Agency agency = client.getAgency();
        TypeAccount typeAccount = typeAccountController.findById();
        bankAccountService.create( bankAccountView.promptBankAccount(client, agency, typeAccount) );
    }

    public List<BankAccount> findAll() {
        return bankAccountService.findAll();
    }
    public void displayAll(){
        findAll().forEach( b -> System.out.println(b));
        appController.getEnterMessage();
    }

    public List<BankAccount> findAllById(){
        return bankAccountService.findAllById(appController.getId());
    }
    public void displayAllById(){
        findAllById().forEach( b -> System.out.println(b));
        appController.getEnterMessage();
    }

    private BankAccount findById() {
        appController.getIdBankAccountTitle();
        return bankAccountService.findById(appController.getId());
    }

    public void update() {
        bankAccountService.update(bankAccountView.promptUpdate(findById()));
    }

    public void delete(){
        bankAccountService.delete(findById());
    }
}
