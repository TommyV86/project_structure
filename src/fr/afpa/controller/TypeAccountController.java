package fr.afpa.controller;

import fr.afpa.model.TypeAccount;
import fr.afpa.service.TypeAccountService;

public class TypeAccountController {
    private final TypeAccountService typeAccountService = new TypeAccountService();
    private final AppController appController = new AppController();

    public TypeAccount findById(){
        appController.getTypeAccountTitle();
        return typeAccountService.findById(appController.getId());
    }
}
