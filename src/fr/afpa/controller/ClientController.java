package fr.afpa.controller;

import fr.afpa.model.Client;
import fr.afpa.service.ClientService;
import fr.afpa.view.ClientView;

import java.util.List;

public class ClientController {
    private final ClientService clientService = new ClientService();
    private final ClientView clientView = new ClientView();
    private final AppController appController = new AppController();

    public void create(){
        clientService.create(clientView.promptCreate());
    }

    public List<Client> findAll() {
        return clientService.findAll();
    }
    public void displayAll() {
        findAll().forEach( c -> System.out.println(c));
        appController.getEnterMessage();
    }

    public Client findById() {
        appController.getIdClientTitle();
        return clientService.findById(appController.getId());
    }
    public void displayOne() {
        System.out.println(findById());
        appController.getEnterMessage();
    }

    public void update() {
        clientService.update(clientView.promptUpdate(findById()));
    }

    public void delete() {
        clientService.delete(findById());
    }
}
