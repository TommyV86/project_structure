package fr.afpa.service;

import fr.afpa.controller.AppController;
import fr.afpa.dao.extension.BankAccountDao;
import fr.afpa.dao.namedQuery.BankAccountNQDao;
import fr.afpa.model.BankAccount;

import java.util.List;

public class BankAccountService {
    private final BankAccountDao bankAccountDao = new BankAccountDao();
    private final BankAccountNQDao bankAccountNQDao = new BankAccountNQDao();
    private final AppController appController = new AppController();
    public void create(BankAccount bankAccount) {
        if (bankAccount == null || bankAccount.getClient() == null || bankAccount.getTypeAccount() == null) {
            System.out.println("insertion de données impossible car les infos sont invalides");
            appController.getEnterMessage();
            return;
        }
        bankAccountDao.create(bankAccount);
    }

    public List<BankAccount> findAll() {
        return bankAccountDao.findAll();
    }

    public BankAccount findById(Long id) {
        return bankAccountDao.findById(id);
    }

    public void update(BankAccount bankAccount) {
        if (bankAccount == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        bankAccountDao.update(bankAccount);
    }

    public void delete(BankAccount bankAccount){
        if (bankAccount == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        bankAccountDao.delete(bankAccount);
    }

    public List<BankAccount> findAllById(Long idClient) {
        return bankAccountNQDao.findAllById(idClient);
    }
}
