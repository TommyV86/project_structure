package fr.afpa.service;

import fr.afpa.controller.AppController;
import fr.afpa.dao.extension.ClientDao;
import fr.afpa.model.Client;

import java.util.List;

public class ClientService {
    private final ClientDao clientDao = new ClientDao();
    private final AppController appController = new AppController();

    public void create(Client client){
        clientDao.create(client);
    }

    public List<Client> findAll() {
        return clientDao.findAll();
    }

    public Client findById(Long id) {
        return clientDao.findById(id);
    }

    public void update(Client client) {
        if (client == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        clientDao.update(client);
    }

    public void delete(Client client) {
        if (client == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        clientDao.delete(client);
    }
}
