package fr.afpa.service;

import fr.afpa.dao.extension.TypeAccountDao;
import fr.afpa.model.TypeAccount;

public class TypeAccountService {
    private final TypeAccountDao typeAccountDao = new TypeAccountDao();
    public TypeAccount findById(Long promptId) {
        return typeAccountDao.findById(promptId);
    }
}
