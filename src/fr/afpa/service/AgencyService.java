package fr.afpa.service;

import fr.afpa.controller.AppController;
import fr.afpa.dao.extension.AgencyDao;
import fr.afpa.model.Agency;

import java.util.List;

public class AgencyService {
    private final AgencyDao agencyDao = new AgencyDao();
    private final AppController appController = new AppController();
    public void create(Agency agency){
        agencyDao.create(agency);
    }

    public List<Agency> findAll() {
        return agencyDao.findAll();
    }

    public Agency find(Long id) {
        return agencyDao.findById(id);
    }

    public void update(Agency agency) {
        if (agency == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        agencyDao.update(agency);
    }

    public void delete(Agency agency) {
        if (agency == null) {
            System.out.println("mauvaise saisie");
            appController.getEnterMessage();
            return;
        }
        agencyDao.delete(agency);
    }

}
