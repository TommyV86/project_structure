package fr.afpa.view;

import fr.afpa.model.Agency;
import fr.afpa.utilitary.ConstantUtilitary;

public class AgencyView {
    private final ConstantUtilitary constantUtilitary = new ConstantUtilitary();
    private Agency agency;

    public Agency promptCreate() {

        agency = new Agency();

        System.out.println("-création d'une agence-");
        System.out.println("Saisir nom de l'agence : ");
        agency.setName(constantUtilitary.SC.nextLine());

        System.out.println("Saisir l'adresse de l'agence :");
        agency.setAddress(constantUtilitary.SC.nextLine());

        return agency;
    }

    public Agency promptUpdate(Agency newAgency) {

        if (newAgency == null) return null;

        System.out.println("-modification des informations de l'agence-");
        System.out.println("Saisir le nouveau nom de l'agence : ");
        System.out.println("nom actuel : " + newAgency.getName());
        newAgency.setName(constantUtilitary.SC.nextLine());

        System.out.println("Saisir la nouvelle adresse de l'agence : ");
        System.out.println("adresse actuelle : " + newAgency.getAddress());
        newAgency.setAddress(constantUtilitary.SC.nextLine());

        return newAgency;
    }

}
