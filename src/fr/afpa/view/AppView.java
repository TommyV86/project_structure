package fr.afpa.view;

import fr.afpa.utilitary.ConstantUtilitary;

public class AppView {

    private final ConstantUtilitary constantUtilitary = new ConstantUtilitary();

    public String menu() {
        System.out.println("\t *** CDA BANQUE - Gestion compte bancaire *** \t");
        System.out.println();

        System.out.println("\t --- AGENCE --- \t");
        System.out.println();
        System.out.println("\t [A] - Creer une agence");
        System.out.println("\t [Z] - Afficher toutes les agences");
        System.out.println("\t [E] - Rechercher une agence");
        System.out.println("\t [R] - Modifier une agence");
        System.out.println("\t [T] - Supprimer une agence");
        System.out.println();

        System.out.println("\t --- CLIENT --- \t");
        System.out.println();
        System.out.println("\t [Q] - Creer un client ");
        System.out.println("\t [S] - Afficher tous les clients");
        System.out.println("\t [D] - Rechercher un client");
        System.out.println("\t [F] - Modifier un client");
        System.out.println("\t [G] - Supprimer un client");
        System.out.println();

        System.out.println("\t --- COMPTE BANCAIRE --- \t");
        System.out.println();
        System.out.println("\t [W] - Creer un compte bancaire");
        System.out.println("\t [X] - Afficher tous les comptes bancaires");
        System.out.println("\t [C] - Afficher liste de compte d'un client");
        System.out.println("\t [V] - Modifier un compte bancaire");
        System.out.println("\t [B] - Supprimer un compte bancaire");
        System.out.println();

        System.out.println();
        System.out.println("\t [K] - Quitter l'application \t");
        System.out.println();

        System.out.print("Choix menu : ");

        return constantUtilitary.SC.nextLine().trim();
    }

    public void promptIdClientTitle(){
        System.out.println("Veuillez saisir l'id du client : ");
    }

    public void promptTypeAccountTitle(){
        System.out.println(
                "Veuillez saisir l'id du type de compte : [1] compte courant | [2] livret a | [3] plan epargne"
        );
    }

    public Long promptId() {
        System.out.println("Saisir l'id : ");
        return Long.parseLong(constantUtilitary.SC.nextLine());
    }

    public void enterMessageToContinue(){
        System.out.println("entrée pour continuer ...");
        constantUtilitary.SC.nextLine();
    }

    public void promptIdBankAccountTitle() {
        System.out.println("Veuillez saisir l'id du compte bancaire : ");
    }
}
