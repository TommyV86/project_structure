package fr.afpa.view;

import fr.afpa.controller.AgencyController;
import fr.afpa.model.Agency;
import fr.afpa.model.Client;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.Date;
import java.time.LocalDate;

public class ClientView {
    private Client client;
    private Agency agency;
    private LocalDate localDate = LocalDate.now();
    private final AgencyController agencyController = new AgencyController();
    private final ConstantUtilitary constantUtilitary = new ConstantUtilitary();


    public Client promptCreate() {

        client = new Client();

        System.out.println("-création d'un client-");
        System.out.println("nom : ");
        client.setLastName(constantUtilitary.SC.nextLine());

        System.out.println("prénom : ");
        client.setFirstName(constantUtilitary.SC.nextLine());

        client.setBirthDate(Date.valueOf(localDate));

        System.out.println("mail : ");
        client.setMail(constantUtilitary.SC.nextLine());

        System.out.println("numéro d'agence : ");
        agency = agencyController.find();
        client.setAgency(agency);

        return client;
    }

    public Client promptUpdate(Client newClient) {

        if (newClient == null) return null;

        System.out.println("-modification des informations d'un client-");
        System.out.println("Saisir le nouveau nom : ");
        System.out.println("nom actuel : " + newClient.getLastName());
        newClient.setLastName(constantUtilitary.SC.nextLine());

        System.out.println("Saisir le nouveau prénom : ");
        System.out.println("prénom actuel : " + newClient.getFirstName());
        newClient.setFirstName(constantUtilitary.SC.nextLine());

        System.out.println("Saisir le nouveau mail : ");
        System.out.println("mail actuel : " + newClient.getMail());
        newClient.setMail(constantUtilitary.SC.nextLine());

        System.out.println("Saisir le nouveau numéro d'agence : ");
        System.out.println("agence actuelle : " + newClient.getAgency().getName());
        System.out.println("id actuel : " + newClient.getAgency().getIdAgency());
        agency = agencyController.find();
        newClient.setAgency(agency);

        return newClient;
    }
}
