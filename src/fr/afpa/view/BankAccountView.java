package fr.afpa.view;

import fr.afpa.model.Agency;
import fr.afpa.model.BankAccount;
import fr.afpa.model.Client;
import fr.afpa.model.TypeAccount;
import fr.afpa.utilitary.ConstantUtilitary;

public class BankAccountView {
    private final ConstantUtilitary constantUtilitary = new ConstantUtilitary();
    public BankAccount promptBankAccount(Client client, Agency agency, TypeAccount typeAccount){

        if(client == null || agency == null) return null;

        for (BankAccount b: client.getBankAccounts()) {
            if(b.getTypeAccount().getType().equals(typeAccount.getType())){
                System.out.println("opération impossible, ce client possède déjà ce compte");
                return null;
            }
        }

        BankAccount bankAccount = new BankAccount();

        bankAccount.setClient(client);
        bankAccount.setAgency(agency);
        bankAccount.setTypeAccount(typeAccount);

        System.out.println("Saisir un solde : ");
        bankAccount.setSolde(Double.parseDouble(constantUtilitary.SC.nextLine()));
        Boolean isNegative = bankAccount.getSolde() <= 0;
        bankAccount.setNegative(isNegative);

        client.getBankAccounts().add(bankAccount);

        return bankAccount;
    }

    public BankAccount promptUpdate(BankAccount bankAccount) {

        if(bankAccount == null) return null;

        System.out.println("Veuillez entrer un montant :");
        bankAccount.setSolde(
                ( Double.parseDouble( constantUtilitary.SC.nextLine() ) + bankAccount.getSolde()  )
        );

        Boolean isNegative = bankAccount.getSolde() <= 0;
        bankAccount.setNegative(isNegative);

        return bankAccount;
    }
}
